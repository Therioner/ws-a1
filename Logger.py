import inspect
import datetime

class Logger(object):
    @staticmethod
    def Log() -> None:
        file = open("calls.log", "a+")

        file.write("Function " + inspect.currentframe().f_back.f_code.co_name + " called at: " + str(datetime.datetime.now()) + "\n")

        file.close()


