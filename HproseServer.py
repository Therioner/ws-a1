import socket
import hprose

class HproseServer(object):
    def Run(self):
        server = hprose.HttpServer(port = 8081);
        server.addFunction(self.Ping)
        server.start()

    def Ping(self):
        systemSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        systemSocket.connect(("8.8.8.8", 80))

        socketAddress =  systemSocket.getsockname()[0]
        systemSocket.close()

        return socketAddress

