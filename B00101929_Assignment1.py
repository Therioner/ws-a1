import threading

from RabbitReceiver import RabbitReceiver
from RPCServer import RPCServer
from HproseServer import HproseServer

hproseServer = HproseServer()
hproseThread = threading.Thread(target = hproseServer.Run)
hproseThread.start()

server = RPCServer()
serverThread = threading.Thread(target = server.Run)
serverThread.start()

rabbitReceiver = RabbitReceiver()
rabbitThread = threading.Thread(target = rabbitReceiver.Run)
rabbitThread.start()
