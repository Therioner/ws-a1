from xmlrpc.server import SimpleXMLRPCServer

class RPCServer(object):
    def Run(self):
        self.server = SimpleXMLRPCServer(("localhost", 8001))

        self.server.register_function(self.CheckTemperature, "CheckTemperature")

        self.server.serve_forever()

    def CheckTemperature(self, temperature: int):
        if temperature >= 0 and temperature <= 10:
            return "cold"
        elif temperature >= 11 and temperature <= 20:
            return "warm"
        else:
            return "unknown temperature parameter"


