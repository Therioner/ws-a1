import requests

ServerUrl = "http://localhost:8080/graphql"

class GraphQLHelper(object):
    @staticmethod
    def GetById(id: str):
        query = "query { studentById(id: \"" + id + "\") { id dateOfBirth studentName } }"
        return requests.post(url = ServerUrl, json = {"query" : query })

    @staticmethod
    def GetByStudentName(studentName: str):
        query = "query { studentByStudentName(studentName: \"" + studentName + "\") { id dateOfBirth studentName } }"
        return requests.post(url = ServerUrl, json = {"query" : query })

    @staticmethod
    def GetByDateOfBirth(dateOfBirth: str):
        query = "query { studentByDateOfBirth(dateOfBirth: \"" + dateOfBirth + "\") { id dateOfBirth studentName } }"
        return requests.post(url = ServerUrl, json = {"query" : query })