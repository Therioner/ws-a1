from flask import Flask
from flask import request
from xmlrpc import client

from GraphQLHelper import GraphQLHelper
from Logger import Logger
from NewsClient import NewsClient

import datetime
import json

app = Flask(__name__)

@app.route('/weather')
def GetWeather() -> str:
    Logger.Log()

    client = NewsClient()

    result = client.WeatherForecasts()

    return json.dumps(ProcessArticles(result["articles"]))

def ProcessArticles(articles: dict) -> list:
    processedArticles = list()

    for article in articles:
        processedArticles.append({ "title": article["title"], "description": article["description"], "name": article["source"]["name"] })

    return processedArticles

@app.route("/updates")
def GetUpdates() -> str:
    Logger.Log()

    updatesFile = open("updates.txt", "r")
    lines = updatesFile.readlines()

    processedLines = dict()
    processedLines["updates"] = list()

    for line in lines:
        processedLines["updates"].append(line)

    return json.dumps(processedLines)

@app.route("/callClient", methods = ["POST"])
def ProcessTemperature() -> str:
    Logger.Log()

    with client.ServerProxy("http://localhost:8001/") as proxy:
        return json.dumps(proxy.CheckTemperature(int(request.form["temperature"])))

@app.route("/innerStudent", methods = ["POST"])
def AddUser() -> str:
    Logger.Log()

    AddUserToFile(request.form["firstName"], request.form["lastName"], request.form["studentNumber"])

    return "Ok"


def AddUserToFile(firstName: str, lastName: str, studentNumber: str) -> None:
    student = dict()

    student["firstName"] = firstName
    student["lastName"] = lastName
    student["studentNumber"] = studentNumber
    student["timestamp"] = str(datetime.datetime.now())

    studentFile = open("users.dat", "a+")
    studentFile.write(json.dumps(student))

@app.route("/ping")
def Ping() -> str:
    Logger.Log()

    return "pong"

@app.route("/student", methods = ["POST"])
def QueryStudents() -> str:
    Logger.Log()

    if "id" in request.form.keys():
        return GraphQLHelper.GetById(request.form["id"]).text
    elif "studentName" in request.form.keys():
        return GraphQLHelper.GetByStudentName(request.form["studentName"]).text
    elif "dateOfBirth" in request.form.keys():
        return GraphQLHelper.GetByDateOfBirth(request.form["dateOfBirth"]).text
    else:
        return "Missing parameters"

@app.route("/test", methods = ["GET"])
def Test() -> str:
    # added this line to create a new merge request
    # added this line to create a new merge request
    # added this line to create a new merge request
    Logger.Log()
    return "Okay!"