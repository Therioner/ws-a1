import pika

class RabbitReceiver(object):
    def Run(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host = "localhost"))
        self.channel = self.connection.channel()

        self.channel.queue_declare(queue = 'demo-queue')

        self.channel.basic_consume('demo-queue', self.Callback, True)
        self.channel.start_consuming()

    def Callback(self, channel, method, properties, body):
        print(body)


